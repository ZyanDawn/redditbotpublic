import praw
import feedparser
from bs4 import BeautifulSoup
import threading
import urllib2  # the lib that handles the url stuff
import time
from collections import defaultdict
import threading
import time
import json
import urllib2
import random
import sys

DONE_CONFIGFILE = "done.txt"
def main(threadName, waitTime):

# Windows based calling by args given from the autostart script on call. If args are detected, this should be the way to go
    try:
        if sys.argv[1] is not None:
            user_agent = sys.argv[1]
            client_id = sys.argv[2]
            client_secret = sys.argv[3]
            username = sys.argv[4]
            password = sys.argv[5]
    except IndexError:
        # This method of credentials taking is used for linux based systems like rasberry pi. If no args are detected, this should be the way to go
        with open('credentials', 'r') as myfile:
            data = myfile.read()

        StringTranslator = []
        StringTranslator = data.split('\n')

        user_agent = StringTranslator[0]
        client_id = StringTranslator[1]
        client_secret = StringTranslator[2]
        username = StringTranslator[3]
        password = StringTranslator[4]

    while True:
        try:

            target_url = 'https://pastebin.com/raw/n8aCxq1F'
            dataFromURL = urllib2.urlopen(target_url)  # it's a file like object and works just like a file

            data = json.load(dataFromURL)

            x = 0
            dataIndexArray1 = []
            dataIndexArray2 = []
            dataIndexArray3 = []

            for dataIndexer1 in data:
                dataIndexArray1.append(dataIndexer1)
                # print dataIndexer1

                x = x + 1
                for dataIndexer2 in data[dataIndexer1]['urls']:
                    # print dataIndexer2
                    dataIndexArray2.append(dataIndexer2)
                    dataIndexArray3.append(dataIndexer2+" "+dataIndexer1+" "+ data[dataIndexer1]['cssClass'])

            threadsList = []
            threadsList = dataIndexArray3

            if (threadName == "CashingThread"):
                CashingThreadStarter = threading.Thread(target=CashingThread,
                                                    args=(user_agent, client_id, client_secret, username, password))
                CashingThreadStarter.start()
                time.sleep(waitTime * 3 / 2)  # increasing to 25 from 20

            if(threadName=="ModeratingThread"):
                ModeratingThread = threading.Thread(target=ModerationThread, args=(user_agent, client_id, client_secret, username, password))
                ModeratingThread.start()
                time.sleep(waitTime/3) # increasing to 25 from 20


            if (threadName == "PostingThread"):
                print(threadsList)
                for threadsInstance in threadsList:
                    threads = []
                    PostingThread = threading.Thread(target=functionToBeExecutedReddit, args=(waitTime-10, threadsInstance.split(" ")[0]
                    , user_agent, client_id, client_secret, username, password
                        , threadsInstance.split(" ")[1], threadsInstance.split(" ")[2]))

                    threads.append(PostingThread)
                    PostingThread.start()
                    # time.sleep(waitTime*3/2) # reducing to 75 from 90
                    time.sleep(waitTime-9)

        except Exception as e:
            s = str(e)
            indexExistingCheckBoolean = False
            print('EXCEPTION                                                                                ')
            print(s)
            pass

def functionToBeExecutedReddit(waitTime, rss_str, user_agent, client_id, client_secret, username, password, tag, cssClass):
    # print tag
    # print cssClass
    start_time = time.time()
    # while True:
    reddit = praw.Reddit(user_agent=user_agent,
                         client_id=client_id, client_secret=client_secret,
                         username=username, password=password)

    subreddit = reddit.subreddit('FallasinSubreddit')
    updateChecker1 = ''
    submit = ''
    SourceNonReddit = ''
    SourceReddit= ''

    t_end = time.time() + waitTime # reducing to 50 from 60
    while time.time() < t_end:
        feed = feedparser.parse(rss_str)
        indexExistingCheckBoolean = True

        try:

            if feed['entries'][0] is not None and indexExistingCheckBoolean is True:
                # print feed['entries'][0]
                title = feed['entries'][0].title
                # print "title "+str(title)
                description = feed['entries'][0].summary,
                # print "description "+str(description)
                url = feed['entries'][0].link,
                # print "url "+str(url)

                # id = feed['entries'][0].id
                updated = feed['entries'][0].updated
                # print "updated " + str(updated)
                # updated_parsed = feed['entries'][0].updated_parsed

                NumberOfComments = ''
                ItemExistingChecker = False
                link = ''

                # print('ITEM ALREADY HERE')
                if updateChecker1 != unicode(updated):
                    print(rss_str)
                    updateChecker1 = unicode(updated)

                    link = unicode(url).encode('utf-8')
                    link = link.replace('(u\'', '', 1)
                    link = link[0: len(link) - 3]

                    if 'https://www.reddit.com/' in link:
                        submission1 = reddit.submission(url=link)
                        NumberOfComments = unicode(submission1.num_comments).encode('utf-8')
                    else:
                        NumberOfComments = ''

                    # The reason the code is slow is because there are such fetches of new subreddit info
                    # This part is broken I believe, but the problem is that either it does not work with other threads or it does not work as a whole
                    # for submission in subreddit.new(limit=100):
                    #     # print('ITEM ALREADY HERE')
                    #     if link in submission.url and submit in submission.title:
                    #         # print('ITEM ALREADY HERE')
                    #         ItemExistingChecker = True
                    #         # break
                    #     # else:
                    #     #     ItemExistingChecker = False
                    #
                    # # print(ItemExistingChecker)
                    # if ItemExistingChecker is False:

                    if 'https://www.reddit.com/' in link:
                        SourceReddit = link.replace('https://www.reddit.com/r/', '')
                        SourceReddit = 'r/' + SourceReddit.split("/")[0]
                        # submit = u' '.join(('[', unicode(SourceReddit), '|', unicode(NumberOfComments), '] ', unicode(title[0:265]))).encode('utf-8')
                        submit = u' '.join(('[', unicode(NumberOfComments), '] ', unicode(title[0:265]))).encode('utf-8')
                        # unicode(SourceReddit), '|',
                        subreddit.submit(submit, url=url)
                    else:
                        SourceNonReddit = link.split("/")[2]
                        SourceNonReddit = SourceNonReddit.replace('www.', '')
                        # submit = u' '.join(('[', unicode(SourceNonReddit), '] ', unicode(title[0:265]))).encode('utf-8')
                        submit = unicode(title[0:265]).encode('utf-8')
                        subreddit.submit(submit, url=url)
                    for submission in subreddit.new(limit=100):
                        if link in submission.url:

                            # print('Replying to: {}' + submission.title)

                            #This is the part that is used for local css updates, but it may no be needed
                            # with open('TheSubredditCSSThatIsAppendedForFlairs.css', 'r') as myfile:
                            #     css = myfile.read()

                            # .linkflairlabel { max-width: none; }.linkflair-green .linkflairlabel {color:green}
                            if 'https://www.reddit.com/' in link:
                                reply = RemoveUsersReddit(BeautifulSoup(feed['entries'][0]["description"],
                                                                        'html.parser').get_text()) + "\n" + "\n" + 'submission.num_comments --> ' + NumberOfComments
                                submission.reply(reply[0:9999])
                                # css = css + ".linkflair-custom .linkflairlabel{color:#ff00ff}"
                                # # print css
                                # subreddit.stylesheet.update(css, 'css')

                                # print tag
                                # print cssClass
                                # cssClassToUse = AutomoderationCssClassCreator(unicode(SourceReddit))
                                # submission.mod.flair(text=unicode(SourceReddit), css_class=cssClassToUse)  # unicode(SourceReddit)
                                submission.mod.flair(text=tag,
                                                 css_class=cssClass)  # unicode(SourceReddit)

                            else:
                                reply = RemoveUsersReddit(BeautifulSoup(feed['entries'][0]["description"], 'html.parser').get_text())
                                submission.reply(reply[0:9999])
                                # css = css + ".linkflair-custom .linkflairlabel{color:#99ccff}"
                                # # print css
                                # subreddit.stylesheet.update(css, 'css')

                                # cssClassToUse = AutomoderationCssClassCreator(unicode(SourceNonReddit))
                                # submission.mod.flair(text=unicode(SourceNonReddit), css_class=cssClassToUse)#unicode(SourceNonReddit)
                                submission.mod.flair(text=tag,
                                                 css_class=cssClass)  # unicode(SourceNonReddit)

                            break

                submit = ''
                SourceNonReddit = ''
                SourceReddit= ''

        except Exception as e:
            s = str(e)
            indexExistingCheckBoolean = False
            print('EXCEPTION                                                                                '+rss_str)
            print(s)
            pass
    # print("--- %s seconds ---" % (time.time() - start_time))

def ModerationThread(user_agent, client_id, client_secret, username, password):
    # print "ModeratingThread"
    start_time = time.time()

    reddit = praw.Reddit(user_agent=user_agent,
                         client_id=client_id, client_secret=client_secret,
                         username=username, password=password)


    subreddit = reddit.subreddit('FallasinSubreddit')

    dic = defaultdict(list)
    lis = []
    coordinatedGeneratedById = []

    i=0
    for submission in subreddit.new(limit=100): 
        # was 1000, changed to 100 for better speed
        # print str(i)+" "+submission.title + " " + submission.url + " " + submission.id
        coordinatedGeneratedById.append(submission.id)
        lis.append(submission.url)
        i=i+1

    for ii, x in enumerate(lis):
        x = unicode(x).encode('utf-8')
        dic[X].append((ii))
    for num, coords in dic.items():
        if len(coords) > 1:
            print "{0} was repeated at coordinates {1}".format(num," ".join(str(x) for x in coords))
            iii = 0
            for numIndex in coords:
                if(iii==0):
                    # print "first - " + coordinatedGeneratedById[numIndex]
                    submission = reddit.submission(id=coordinatedGeneratedById[numIndex])
                    submission.delete()
                    # pass
                if (iii >= 1):
                    # print "second+ - " + coordinatedGeneratedById[numIndex]
                    pass
                    # submission = reddit.submission(id=coordinatedGeneratedById[numIndex])
                    # submission.delete()

                iii=iii+1


    # print("--- %s seconds ---" % (time.time() - start_time))

def CashingThread(user_agent, client_id, client_secret, username, password):
    print "CashingThread"
    submissionArray = []
    reddit = praw.Reddit(user_agent=user_agent,
                         client_id=client_id, client_secret=client_secret,
                         username=username, password=password)


    subreddit = reddit.subreddit('FallasinSubreddit')

    f = open('data', 'r+')
    f.truncate()
    f = open('data1', 'r+')
    f.truncate()

    for submission in subreddit.new(limit=100):
        # print submission

        with open('data', 'a') as the_file:
            the_file.write(str(submission) + "\n")
    with open('data', 'r') as the_file:
        # if(the_file.read() not in submissionArray):

        texSpliter = the_file.read().split("\n")
        for indexObject in texSpliter:

            # print len(texSpliter)
            # print texSpliter
            print indexObject

            # submission1 = reddit.submission(id=texSpliter[len(texSpliter)-2])
            submission1 = reddit.submission(id=indexObject)
            submissionArray.append(submission1)

            print submission1.url

            with open('data1', 'a') as the_file:
                the_file.write(str(submission1.url) + "\n")

                # print submissionArray.__getitem__(len(submissionArray)-1).url

    # for submission1 in submissionArray:
    #     print submission1.url


    # print submissionArray[0].url

    # submission = reddit.submission(id='3g1jfi')

def AutomoderationCssClassCreator(typeOfPost):
    # print typeOfPost
    cssClassToUse = ""
    if "kaldata.com" in typeOfPost:
        cssClassToUse = "orange"
    # elif typeOfPost.find("twitter.com") == 1:
    elif "twitter.com" in typeOfPost:
        cssClassToUse = "green"
    # elif typeOfPost.find("boards.4chan.org") == 1:
    elif "boards.4chan.org" in typeOfPost:
        cssClassToUse = "red"
    # elif typeOfPost.find("r/hearthstone") == 1:
    elif "r/hearthstone" in typeOfPost:
        cssClassToUse = "purple"
    # elif typeOfPost.find("r/") == 1:
    elif "r/" in typeOfPost:
        cssClassToUse = "brown"
    # elif typeOfPost.find("channelfireball.com") == 1:
    elif "channelfireball.com" in typeOfPost:
        cssClassToUse = "cyan"
    elif "tech" in typeOfPost:
        cssClassToUse = "redcustom1"
    elif "gaming" in typeOfPost:
        cssClassToUse = "redcustom2"
    else:
        cssClassToUse = "custom"
    # print cssClassToUse
    return cssClassToUse

#The functions bellow are here just in case read/write from the device is implemented as the main way of comparing
# new posts to old in order to search for dublication
def read_config_done():
    done = set()
    try:
        with open(DONE_CONFIGFILE, "r") as f:
            for line in f:
                if line.strip():
                    done.add(line.strip())
    except OSError:
        pass
    return done

def write_config_done(done):
    with open(DONE_CONFIGFILE, "w") as f:
        for d in done:
            if d:
                f.write(d + "\n")

def RemoveUsersReddit(str):
    word2 = ''
    for word in str.split():
        word2 += word.replace('/u/', '') + ' '
    return word2

if __name__ == '__main__':
    #Cashing thread calls are made for the local cashing idea that proves, for now, to be slower and thus inefficient
    waitTime = 50
    ModeratingThreadString = "ModeratingThread"
    PostingThreadString = "PostingThread"
    # CashingThreadString = "CashingThread"
    LauncherModeratingThread = threading.Thread(target=main, args=(ModeratingThreadString, waitTime))
    LauncherPostingThread = threading.Thread(target=main, args=(PostingThreadString, waitTime))
    # LauncherCashingThread = threading.Thread(target=main, args=(CashingThreadString, waitTime))
    LauncherPostingThread.start()
    LauncherModeratingThread.start()
    # LauncherCashingThread.start()
